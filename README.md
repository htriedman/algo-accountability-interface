# algo-accountability-interface

An interface that helps contributors to WMF's ML infrastructure write model cards and datasheets for their models and datasets. It will first implement a questionnaire to figure out which kind of algorithmic accountability sheet the contributor will 